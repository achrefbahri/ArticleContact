import React from "react";
import ModalView from "./ModalView"
import contact_data from '../data/Contact.json';
import '../App.css';

export default class Contact extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            contact_data: contact_data,
            modalIsOpen: false,
            curentobject: '',
            search: this.props.search,
        };

    }


    componentWillReceiveProps(nextProps) {
       this.setState({ curentobject: '', search: nextProps.search })   
            console.log("searchhhh will ",nextProps.search);



    }



    changeCurrentContact(contact) {
        this.setState({ curentobject: contact })

    }
    render() {
        var findContact = false

        return (<div className="list">

            <h3> Liste Contact </h3>

            <table><thead>
                <tr>
                    <th>APNR</th>
                    <th>NACHNAME</th>
                    <th>EMAIL</th>
                    <th>TELEFON</th>
                    <th>GEB</th>
                    <th>ANREDE</th>
                    <th>Action</th>
                </tr>
            </thead><tbody>
                    {
                        this.state.contact_data.Data.map((cont, key) => {
                            findContact = false
                        if (this.state.search == "all") { //console.log("true 2");
                        findContact = true }
                        else {
                            {    
                                Object.keys(cont).map(item => {
                                
                                    if (((cont[item].toString().toUpperCase()).search(this.state.search.toString().toUpperCase()))>= 0)
                                        findContact = true
                                })
                            }
                        }

                        if (findContact) {
                            return (
                                <tr key={key}>
                                    <td>{cont.APNR}</td>
                                    <td>{cont.NACHNAME}</td>
                                    <td><a href={"mailto:" + cont.EMAIL}>{cont.EMAIL}</a></td>
                                    <td>{cont.TELEFON}</td>
                                    <td>{cont.GEB}</td>
                                    <td>{cont.ANREDE}</td>
                                    <td onClick={this.changeCurrentContact.bind(this, cont)}><span className="btn btn-info">Detail</span></td>
                                </tr>
                            )

                        }
                    })}
                </tbody>
            </table>
            {this.state.curentobject != '' && <ModalView modalIsOpen={true} curentobject={this.state.curentobject} />}
        </div>)
    }
}