import React from "react";
export default class AddArticle extends React.Component {

    constructor(props) {
        super(props)
        this.state = { ARTIKEL: '', ARTIKELBEZEICHNUNG: '', SPERRE: '' };
    }
    submit = () => {
        const newArticle = { 'ARTIKEL': this.state.ARTIKEL, 'ARTIKELBEZEICHNUNG': this.state.ARTIKELBEZEICHNUNG, 'SPERRE': this.state.SPERRE }
        this.props.addNewArticle(newArticle)
    }
    handlechange = (event) => {
        event.preventDefault();
        this.setState({ [event.target.name]: event.target.value });
    }

    render() {
        return (
            <div>

                <h3> Add Article </h3>
                <div className="form-group">
                    <label>ARTIKEL</label>
                    <input className="form-control" onChange={this.handlechange} value={this.state.ARTIKEL} type="text" name="ARTIKEL" />
                </div>
                <div className="form-group">
                    <label>ARTIKELBEZEICHNUNG</label>
                    <input className="form-control" value={this.state.ARTIKELBEZEICHNUNG} onChange={this.handlechange} type="text" name="ARTIKELBEZEICHNUNG" />
                </div>
                <div className="form-group">
                    <label>SPERRE</label>
                    <input type="text" value={this.state.SPERRE} onChange={this.handlechange} className="form-control" name="SPERRE" />
                </div>
                <button onClick={this.submit} className="btn btn-primary" >Submit</button>
            </div>
        )
    }
}