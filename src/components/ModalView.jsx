import React from "react"
import Modal from 'react-responsive-modal';
//import '../css/animation.css';
import '../App.css';

export default class Modalview extends React.Component {
    constructor(props) {
        super(props)
        this.state = { modalIsOpen: this.props.modalIsOpen, curentobject: this.props.curentobject }
    }

    onOpenModal = () => {
        this.setState({ modalIsOpen: true });
    };

    onCloseModal = () => {
        this.setState({ modalIsOpen: false });
    };
    componentWillMount()
    {

    }

    componentWillReceiveProps(nextState) {
        this.setState({ modalIsOpen: true, curentobject: nextState.curentobject })
        { console.log(nextState.curentobject.ARTIKEL,"teest")}
    }
    componentWillMount() {

    }
    render() {
        return (
            <Modal
                open={this.state.modalIsOpen}
                onClose={this.onCloseModal.bind(this)}
                className="Modal"
                little>
                <h2>Detail</h2>
                <div className="list">

                    {Object.keys(this.state.curentobject).map((obj,key) =>
                        <div key={key} ><label>{obj} :</label> {this.state.curentobject[obj]}</div>)}

                    <button className="btn btn-danger" onClick={this.onCloseModal.bind(this)}>close</button>
                </div>
            </Modal>

        )
    }
}