import AddArticle from "./AddArticle";
import article_data from '../data/Article.json';
import ModalView from "./ModalView";
import React, { Fragment } from 'react';
export default class Article extends React.Component {
    constructor(props) {
        super(props);
        let Articles = JSON.parse(localStorage.getItem('Articles')) ? JSON.parse(localStorage.getItem('Articles')) : { 'listArticle': [] };
        localStorage.setItem("Articles", JSON.stringify(Articles));
        {       /*    Articles=(Articles['listArticle']);
    Articles=(JSON.parse(articles));*/}
        this.state = {
            article_data: [...article_data.Data, ...Articles.listArticle],
            search: this.props.search,
            addArticle: false,
            table: { id: 1, square: 2 },
            curentobject: ''
        };

    }

    componentWillReceiveProps(nextProps) {
        this.setState({ addArticle: false, curentobject: '',search:nextProps.search })
        console.log("article search ",nextProps.search)
    }


    addArt = () => {

        this.setState({ curentobject: '' })
        this.setState({ addArticle: true })
    }
    changeCurentArticle = (index) => {
        this.setState({ curentobject: this.state.article_data[index] })
    }

    addNewArticle = (article) => {
        let Articles = JSON.parse(localStorage.getItem('Articles'));

        Articles.listArticle ? localStorage.setItem("Articles", JSON.stringify({ "listArticle": [...Articles.listArticle, article] })) : localStorage.setItem("Articles", JSON.stringify({ "listArticle": [article] }))
        Articles = JSON.parse(localStorage.getItem('Articles'));
        this.setState({ article_data: [...this.state.article_data, ...Articles.listArticle] })
        this.setState({ addArticle: false })
    }

    render() {
var findArticle=false;
        return (
            <div className="list">
                {
                    !this.state.addArticle && <Fragment>
                        <h3> liste Article</h3>
                        {
                            <button id="btn" className="btn btn-info btn-lg btn-block" onClick={this.addArt}>  <i className="material-icons">add</i> </button>
                        }
                        <table className="tab">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>APNR</th>
                                    <th>ARTIKELBEZEICHNUNG</th>
                                    <th>SPERRE</th>
                                    <th>EINHEIT</th>
                                    <th>Détail</th>

                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.article_data.map((art, key) => {
                                        findArticle=false;
                                        if(this.state.search=="all")
                                      {  findArticle=true;}
                                        else
                                        {

                                            Object.keys(art).map(item=>{  
                                                if (art[item])
                                                { if (((art[item].toString().toUpperCase()).search(this.state.search.toUpperCase()))>= 0)
                                                 findArticle = true}
                                        })
                                        }
                                       
                                        if (findArticle) {


                                            return (<tr key={key}>
                                                <td>{key + 1}</td>
                                                <td>{art.ARTIKEL}</td>
                                                <td>{art.ARTIKELBEZEICHNUNG}</td>
                                                <td>{art.SPERRE}</td>

                                                <td>{art.EINHEIT}</td>
                                                <td onClick={this.changeCurentArticle.bind(this, key)}><span className="btn btn-info">Detail</span></td>
                                            </tr>)
                                        }

                                    }


                                    )
                                }
                            </tbody>
                        </table></Fragment>}

                {
                    this.state.addArticle &&
                    <AddArticle addNewArticle={this.addNewArticle} />
                    
                   }{ console.log(this.state.curentobject.ARTIKEL)}
                   {this.state.curentobject != '' && <ModalView modalIsOpen={true} curentobject={this.state.curentobject} />}

            </div>

        )
    }
}