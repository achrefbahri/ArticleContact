import React, { Component } from 'react';

import './App.css';

import contact from './data/Contact.json';
import Article from './components/Article';
import Contact from './components/Contact';

console.log('article', contact);
class App extends Component {
  constructor(props) {
    super(props)
    this.state = { searchArt: '', searchCont: '' }
  }
  clickArticle = () => {

    this.setState({ searchArt: "all", searchCont: '' });
  }




  clickContact = () => {

    this.setState({ searchArt: "", searchCont: 'all' });
  }



  contact_article = () => {
    if (this.state.searchArt.length > 0 && this.state.searchCont.length > 0) {
      return (<React.Fragment><Contact search={this.state.searchCont} /><Article search={this.state.searchArt} /></React.Fragment>)

        
    }
    if (this.state.searchCont.length > 0) {
      return <Contact search={this.state.searchCont} />;
    }
    if (this.state.searchArt.length > 0)
      return <Article search={this.state.searchArt} />
  }

  handlesearch = event => {
    let v = event.target.value;
    this.setState({ searchArt: v, searchCont: v })

  }


  render() {
    return (
      <div>
        <input type='text' className="form-control" id="input" onKeyUp={this.handlesearch} name='search' placeholder='search' />

        <div className="list-btn">
          <input type="button" id="btn" className="btn btn-primary" onClick={this.clickContact} className="btn btn-primary" value="contact" name="cont" />
          <input type="button" id="btn" className="btn btn-primary" onClick={this.clickArticle} value="article" name="art" />
          {this.contact_article()}

        </div>
      </div>
    );
  }
}


export default App;
